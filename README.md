**Magento 2 Exercise – Phone Book Application**
---

## Magento PhoneBook module


1. Folder app/code/Exercise/PhoneBook
2. Run the following command in Magento 2 root folder:

   - php bin/magento setup:upgrade
   - php bin/magento setup:di:compile
   - php bin/magento setup:static-content:deploy
   - php bin/magento c:c
   - php bin/magento c:f

4. Log in to Admin.
5. Click "Magento Exercise" menu on the left sidebar.
