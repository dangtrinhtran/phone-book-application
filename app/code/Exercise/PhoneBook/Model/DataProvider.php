<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Model;

use Exercise\PhoneBook\Model\ResourceModel\PhoneBook\CollectionFactory as PhoneBookCollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 * @package Exercise\PhoneBook\Model
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * DataProvider Constructor.
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param PhoneBookCollectionFactory $phoneBookCollection
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        PhoneBookCollectionFactory $phoneBookCollection,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $phoneBookCollection->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();
        foreach ($items as $phoneBook) {
            $this->loadedData[$phoneBook->getId()]['phonebook'] = $phoneBook->getData();
        }
        $data = $this->dataPersistor->get('phonebook');
        if (!empty($data)) {
            $phoneBook = $this->collection->getNewEmptyItem();
            $phoneBook->setData($data);
            $this->loadedData[$phoneBook->getId()]['phonebook'] = $phoneBook->getData();
            $this->dataPersistor->clear('phonebook');
        }

        return $this->loadedData;
    }
}
