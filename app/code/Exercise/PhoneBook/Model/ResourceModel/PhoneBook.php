<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Class PhoneBook
 * @package Exercise\PhoneBook\Model\ResourceModel
 */
class PhoneBook extends AbstractDb
{
    /**
     * DB connection
     *
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * PhoneBook Constructor.
     *
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model. Get table_name from config
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exercise_phone_book', 'id');
        $this->connection = $this->getConnection();
    }
}
