<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Model\ResourceModel\PhoneBook;

/**
 * PhoneBook Collection
 * @package Exercise\PhoneBook\Model\ResourceModel\PhoneBook
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'exercise_phonebook_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'phonebook_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exercise\PhoneBook\Model\PhoneBook::class, \Exercise\PhoneBook\Model\ResourceModel\PhoneBook::class);
    }
}
