<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Model;

use Exercise\PhoneBook\Model\PhoneBookFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Repository for performing CRUD operations for a phone_book.
 * @package Exercise\PhoneBook\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PhoneBookRepository implements \Exercise\PhoneBook\Api\PhoneBookRepositoryInterface
{
    /**
     * @var \Exercise\PhoneBook\Model\ResourceModel\PhoneBook
     */
    protected $phoneBookResource;

    /**
     * @var PhoneBookFactory
     */
    protected $phoneBookFactory;

    /**
     * PhoneBookRepository Constructor.
     *
     * @param \Exercise\PhoneBook\Model\ResourceModel\PhoneBook $phoneBookResource
     * @param PhoneBookFactory $phoneBookFactory
     */
    public function __construct(
        \Exercise\PhoneBook\Model\ResourceModel\PhoneBook $phoneBookResource,
        PhoneBookFactory $phoneBookFactory
    ) {
        $this->phoneBookResource = $phoneBookResource;
        $this->phoneBookFactory = $phoneBookFactory;
    }

    /**
     * @inheritdoc
     */
    public function save(
        \Exercise\PhoneBook\Api\Data\PhoneBookInterface $phoneBook
    ): \Exercise\PhoneBook\Api\Data\PhoneBookInterface
    {
        try {
            $this->phoneBookResource->save($phoneBook);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Phone Book: %1',
                $exception->getMessage()
            ));
        }

        return $phoneBook;
    }

    /**
     * @inheritdoc
     */
    public function getById(int $phoneBookId): \Exercise\PhoneBook\Api\Data\PhoneBookInterface
    {
        $phoneBook = $this->phoneBookFactory->create();
        $this->phoneBookResource->load($phoneBook, $phoneBookId);
        if (!$phoneBook->getId()) {
            throw new NoSuchEntityException(__('The Phone Book with id "%1" does not exist.', $phoneBookId));
        }

        return $phoneBook;
    }

    /**
     * @inheritdoc
     */
    public function delete(\Exercise\PhoneBook\Api\Data\PhoneBookInterface $phoneBook): bool
    {
        try {
            $phoneBookModel = $this->phoneBookFactory->create();
            $this->phoneBookResource->load($phoneBookModel, $phoneBook->getId());

            $this->phoneBookResource->delete($phoneBookModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                __('The Phone Book with "%1" ID can\'t be deleted.', $phoneBook->getId()),
                $exception->getMessage()
            ));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById(int $phoneBookId): bool
    {
        return $this->delete($this->getById($phoneBookId));
    }
}
