<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Model;

use Magento\Framework\Model\AbstractModel;
use Exercise\PhoneBook\Api\Data\PhoneBookInterfaceFactory;
use Exercise\PhoneBook\Api\Data\PhoneBookInterface;

/**
 * Class PhoneBook
 * @package Exercise\PhoneBook\Model
 */
class PhoneBook extends AbstractModel implements PhoneBookInterface
{
    /**
     * @var PhoneBookInterfaceFactory
     */
    protected $phoneBookDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * PhoneBook Constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\PhoneBook $resource
     * @param PhoneBookInterfaceFactory $phoneBookDataFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exercise\PhoneBook\Model\ResourceModel\PhoneBook $resource,
        PhoneBookInterfaceFactory $phoneBookDataFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->phoneBookDataFactory = $phoneBookDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\PhoneBook::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?string
    {
        return parent::getData(self::KEY_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id): PhoneBookInterface
    {
        return $this->setData(self::KEY_ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt(): ?string
    {
        return $this->getData(self::KEY_CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt(string $createdAt): PhoneBookInterface
    {
        return $this->setData(self::KEY_CREATED_AT, $createdAt);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt(): ?string
    {
        return $this->getData(self::KEY_UPDATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt(string $updatedAt): PhoneBookInterface
    {
        return $this->setData(self::KEY_UPDATED_AT, $updatedAt);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail(): string
    {
        return $this->getData(self::KEY_EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail(string $email): PhoneBookInterface
    {
        return $this->setData(self::KEY_EMAIL, $email);
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstname(): string
    {
        return $this->getData(self::KEY_FIRSTNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstname(string $firstname): PhoneBookInterface
    {
        return $this->setData(self::KEY_FIRSTNAME, $firstname);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastname(): string
    {
        return $this->getData(self::KEY_LASTNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastname(string $lastname): PhoneBookInterface
    {
        return $this->setData(self::KEY_LASTNAME, $lastname);
    }

    /**
     * {@inheritdoc}
     */
    public function getTelephone(): ?string
    {
        return $this->getData(self::KEY_TELEPHONE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTelephone(string $telephone): PhoneBookInterface
    {
        return $this->setData(self::KEY_TELEPHONE, $telephone);
    }
}
