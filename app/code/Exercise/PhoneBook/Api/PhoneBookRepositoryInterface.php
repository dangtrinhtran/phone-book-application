<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exercise\PhoneBook\Api;

/**
 * PhoneBook CRUD interface.
 * @package Exercise\PhoneBook\Api
 * @api
 * @since 1.0.0
 */
interface PhoneBookRepositoryInterface
{
    /**
     * Create or update a phone_book.
     *
     * @param \Exercise\PhoneBook\Api\Data\PhoneBookInterface $phoneBook
     * @return \Exercise\PhoneBook\Api\Data\PhoneBookInterface
     * @throws \Magento\Framework\Exception\InputException If bad input is provided
     * @throws \Magento\Framework\Exception\State\InputMismatchException If the provided email is already used
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Exercise\PhoneBook\Api\Data\PhoneBookInterface $phoneBook): Data\PhoneBookInterface;

    /**
     * Get phone_book by phone_book ID.
     *
     * @param int $phoneBookId
     * @return \Exercise\PhoneBook\Api\Data\PhoneBookInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If phone_book with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById(int $phoneBookId): Data\PhoneBookInterface;

    /**
     * Delete phone_book.
     *
     * @param \Exercise\PhoneBook\Api\Data\PhoneBookInterface $phoneBook
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Exercise\PhoneBook\Api\Data\PhoneBookInterface $phoneBook): bool;

    /**
     * Delete phone_book by phone_book ID.
     *
     * @param int $phoneBookId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById(int $phoneBookId): bool;
}
