<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exercise\PhoneBook\Api\Data;

/**
 * PhoneBook Interface.
 * @package Exercise\PhoneBook\Api\Data
 * @api
 */
interface PhoneBookInterface
{
    /**#@+
     * Constants defined for keys of the data array. Identical to the name of the getter in snake case
     */
    const KEY_ID         = 'id';
    const KEY_CREATED_AT = 'created_at';
    const KEY_UPDATED_AT = 'updated_at';
    const KEY_EMAIL      = 'email';
    const KEY_FIRSTNAME  = 'firstname';
    const KEY_LASTNAME   = 'lastname';
    const KEY_TELEPHONE  = 'telephone';
    /**#@-*/

    /**
     * Get phone_book id
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set phone_book id
     *
     * @param mixed $id
     * @return $this
     */
    public function setId($id): PhoneBookInterface;

    /**
     * Get created at time
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Set created at time
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt): PhoneBookInterface;

    /**
     * Get updated at time
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string;

    /**
     * Set updated at time
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt): PhoneBookInterface;

    /**
     * Get email address
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Set email address
     *
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): PhoneBookInterface;

    /**
     * Get first name
     *
     * @return string
     */
    public function getFirstname(): string;

    /**
     * Set first name
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname(string $firstname): PhoneBookInterface;

    /**
     * Get last name
     *
     * @return string
     */
    public function getLastname(): string;

    /**
     * Set last name
     *
     * @param string $lastname
     * @return $this
     */
    public function setLastname(string $lastname): PhoneBookInterface;

    /**
     * Get telephone
     *
     * @return string|null
     */
    public function getTelephone(): ?string;

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return $this
     */
    public function setTelephone(string $telephone): PhoneBookInterface;
}
