<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exercise\PhoneBook\Controller\Adminhtml;

use Exercise\PhoneBook\Model\PhoneBookFactory;
use Exercise\PhoneBook\Api\PhoneBookRepositoryInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\App\ObjectManager;
use Exercise\PhoneBook\Model\ResourceModel\PhoneBook\CollectionFactory as PhoneBookCollectionFactory;

/**
 * PhoneBook Controller
 * @package Exercise\PhoneBook\Controller\Adminhtml
 */
abstract class PhoneBook extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see \Magento\Backend\App\Action\_isAllowed()
     */
    const ADMIN_RESOURCE = 'Exercise_PhoneBook::phone_book';

    /**
     * @var PhoneBookRepositoryInterface
     */
    protected $phoneBookRepository;

    /**
     * @var PhoneBookFactory
     */
    protected $phoneBookFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var PhoneBookCollectionFactory
     */
    protected $phoneBookCollectionFactory;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * Initialize PhoneBook Controller
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param PhoneBookRepositoryInterface $phoneBookRepository
     * @param PhoneBookFactory $phoneBookFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param DataPersistorInterface $dataPersistor
     * @param Filter $filter
     * @param PhoneBookCollectionFactory $phoneBookCollectionFactory
     * @param Validator|null $formKeyValidator
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        PhoneBookRepositoryInterface $phoneBookRepository,
        PhoneBookFactory $phoneBookFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        Filter $filter,
        PhoneBookCollectionFactory $phoneBookCollectionFactory,
        Validator $formKeyValidator = null
    ) {
        $this->phoneBookRepository = $phoneBookRepository;
        $this->phoneBookFactory = $phoneBookFactory;
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->dataPersistor = $dataPersistor;
        $this->filter = $filter;
        $this->phoneBookCollectionFactory = $phoneBookCollectionFactory;
        $this->formKeyValidator = $formKeyValidator ?: ObjectManager::getInstance()->get(Validator::class);
    }
}
