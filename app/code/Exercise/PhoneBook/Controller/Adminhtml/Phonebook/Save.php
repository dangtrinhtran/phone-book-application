<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Controller\Adminhtml\Phonebook;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Save PhoneBook action.
 * @package Exercise\PhoneBook\Controller\Adminhtml\Phonebook
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Exercise\PhoneBook\Controller\Adminhtml\PhoneBook implements HttpPostActionInterface
{
    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute(): \Magento\Framework\Controller\ResultInterface
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->getRequest()->isPost()
            || !$this->formKeyValidator->validate($this->getRequest())
        ) {
            return $resultRedirect->setPath('*/*/');
        }
        $postData = $this->getRequest()->getPostValue();
        if ($postData && isset($postData['phonebook'])) {
            $data = $postData['phonebook'];
            if (empty($data['id'])) {
                $data['id'] = null;
            }
            $model = $this->phoneBookFactory->create();
            $id = $data['id'];
            if ($id) {
                try {
                    $model = $this->phoneBookRepository->getById((int)$id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This PhoneBook no longer exists.'));

                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);
            try {
                $this->phoneBookRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the PhoneBook.'));
                $this->dataPersistor->clear('phonebook');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the PhoneBook.'));
            }

            $this->dataPersistor->set('phonebook', $data);

            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
