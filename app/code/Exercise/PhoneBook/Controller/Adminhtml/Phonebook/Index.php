<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Controller\Adminhtml\Phonebook;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

/**
 * Class Index
 * @package Exercise\PhoneBook\Controller\Adminhtml\Phonebook
 */
class Index extends \Exercise\PhoneBook\Controller\Adminhtml\PhoneBook implements HttpGetActionInterface
{
    /**
     * Phone book listing grid page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute(): \Magento\Backend\Model\View\Result\Page
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exercise_PhoneBook::phone_book');
        $resultPage->getConfig()->getTitle()->prepend(__('Phone Book Listing'));

        return $resultPage;
    }
}
