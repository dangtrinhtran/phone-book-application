<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Controller\Adminhtml\Phonebook;

use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Create PhoneBook page action.
 * @package Exercise\PhoneBook\Controller\Adminhtml\Phonebook
 */
class NewAction extends \Exercise\PhoneBook\Controller\Adminhtml\PhoneBook implements HttpGetActionInterface
{
    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute(): \Magento\Backend\Model\View\Result\Forward
    {
        $resultForward = $this->resultForwardFactory->create();

        return $resultForward->forward('edit');
    }
}
