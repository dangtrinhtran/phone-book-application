<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Controller\Adminhtml\Phonebook;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Delete
 * @package Exercise\PhoneBook\Controller\Adminhtml\Phonebook
 */
class Delete extends \Exercise\PhoneBook\Controller\Adminhtml\PhoneBook implements HttpGetActionInterface
{
    /**
     * Delete PhoneBook.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute(): \Magento\Backend\Model\View\Result\Redirect
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $this->phoneBookRepository->deleteById((int)$id);
                $this->messageManager->addSuccessMessage(__('You deleted the phone book.'));
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The phone book no longer exists.'));

                return $resultRedirect->setPath('exercise/phonebook/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('exercise/phonebook/edit', ['id' => $id]);
            }
        }

        return $resultRedirect->setPath('exercise/phonebook/index');
    }
}
