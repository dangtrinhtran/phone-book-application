<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Controller\Adminhtml\Phonebook;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Edit PhoneBook page action.
 * @package Exercise\PhoneBook\Controller\Adminhtml\Phonebook
 */
class Edit extends \Exercise\PhoneBook\Controller\Adminhtml\PhoneBook implements HttpGetActionInterface
{
    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction(): \Magento\Backend\Model\View\Result\Page
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exercise_PhoneBook::phone_book')
            ->addBreadcrumb(__('Phone Book App'), __('Phone Book App'))
            ->addBreadcrumb(__('Manage PhoneBooks'), __('Manage PhoneBooks'));

        return $resultPage;
    }

    /**
     * Edit PhoneBook page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create(\Exercise\PhoneBook\Model\PhoneBook::class);

        // 2. Initial checking
        if ($id) {
            try {
                $model = $this->phoneBookRepository->getById((int)$id);
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('This PhoneBook no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('exercise/phonebook/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('exercise/phonebook/edit', ['id' => $id]);
            }
        }

        // 3. Build edit form
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit PhoneBook') : __('New PhoneBook'),
            $id ? __('Edit PhoneBook') : __('New PhoneBook')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('PhoneBook'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getFirstname() : __('New PhoneBook'));

        return $resultPage;
    }
}
