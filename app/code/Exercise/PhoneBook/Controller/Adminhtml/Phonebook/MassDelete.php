<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Exercise\PhoneBook\Controller\Adminhtml\Phonebook;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class MassDelete
 * @package Exercise\PhoneBook\Controller\Adminhtml\Phonebook
 */
class MassDelete extends \Exercise\PhoneBook\Controller\Adminhtml\PhoneBook implements HttpPostActionInterface
{
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute(): \Magento\Backend\Model\View\Result\Redirect
    {
        $collection = $this->filter->getCollection($this->phoneBookCollectionFactory->create());
        $phoneBookDeleted = 0;
        foreach ($collection->getAllIds() as $phoneBookId) {
            $this->phoneBookRepository->deleteById((int)$phoneBookId);
            $phoneBookDeleted++;
        }

        if ($phoneBookDeleted) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $phoneBookDeleted));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
